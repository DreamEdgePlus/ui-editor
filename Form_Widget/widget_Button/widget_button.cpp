
#include "widget_button.h"


Widget_Button::Widget_Button()
{
    container = false; //定义为非容器


    //设置属性
    propertyMsg t_strItem;
    t_strItem.name = "按钮标题";
    t_strItem.type = propertyType::String;
    t_strItem.value_String = "";
    t_strItem.enable = true;
    t_strItem.sign = "基本属性";
    propertyList.append(t_strItem);


    //设置事件信息
    widgetEventMsg t_eventMsg;
    t_eventMsg.eventName = "onClick";
    t_eventMsg.eventReturn = "void";
    t_eventMsg.eventArgs = "";
    eventList.append(t_eventMsg);
}

Widget_Button::~Widget_Button()
{
    if(widget_but != nullptr){
        delete widget_but;
        widget_but = nullptr;
    }
}


//获取控件实例
QWidget *Widget_Button::getWidgetInstance()
{
    if(widget_but == nullptr){
        widget_but = new QPushButton;
        widget_but->setObjectName("按钮");
        widget_but->setText("按钮");
        widget_but->setGeometry(0,0,100,40);

//        t_strItem.name = "顶边";
//        t_strItem.type = propertyType::Int;
//        t_strItem.value_Int = 0;
//        t_strItem.enable = true;
//        t_strItem.sign = "位置";
//        propertyList.push_front(t_strItem);
    }
    return widget_but;
}


//获取构建代码
QString Widget_Button::getCodeStr()
{
    return "";
}


//当前窗口尺寸被改变,newSizeRect为建议改变的值
void Widget_Button::event_onWidgetSizeChange(QRect newSizeRect)
{
    if(widget_but == nullptr) return;
    widget_but->setGeometry(newSizeRect);
    propertyList[0].value_Int = newSizeRect.top(); //刷新顶边属性
    propertyList[1].value_Int = newSizeRect.left(); //刷新左边属性
    propertyList[2].value_Size = QSize(newSizeRect.width(),newSizeRect.height()); //刷新默认宽高

    qDebug() << "位置被移动";
}


//当属性被改变
void Widget_Button::event_onSetChange()
{
    return;
}


//当子项窗口被加入，此事件将在“container = true”后生效
void Widget_Button::event_onChildJoin(QWidget *itemWidget)
{
    return;
}

