
#ifndef WIDGET_BUTTON_H
#define WIDGET_BUTTON_H

#include "widget_Button_global.h"
#include "QPushButton"

#include "../../Widget/Widget_Base.h"

class Widget_Button : public Widget_Base
{
public:
    Widget_Button();
    ~Widget_Button();


private:
    QPushButton* widget_but = nullptr;


public:
    QWidget* getWidgetInstance() override; //获取控件实例
    QString getCodeStr() override; //获取代码字符串


    void event_onWidgetSizeChange(QRect newSizeRect) override;//当前窗口尺寸被改变,newSizeRect为建议改变的值
    void event_onSetChange() override; //当属性被改变
    void event_onChildJoin(QWidget* itemWidget) override; //当子项窗口被加入，此事件将在“container = true”后生效
};



extern "C" WIDGET_BUTTON_EXPORT uiWidgetMsg getWidgetMsg(){
    uiWidgetMsg t_msg;
    t_msg.pix = QPixmap(":/icon/icon/Button_16x.png");
    t_msg.name = "按钮";
    t_msg.className = "QPushButton";
    t_msg.tip = "用于按下触发事件的按钮";
    return t_msg;
}

extern "C" WIDGET_BUTTON_EXPORT Widget_Button* getInstance(){
    return new Widget_Button;
}


#endif // WIDGET_BUTTON_H
