
#ifndef GLOBALMSG_H
#define GLOBALMSG_H

#include "QPixmap"
#include "QLibrary"



//UI控件的Msg信息
struct uiWidgetMsg{
    QPixmap pix;  //控件图标，仅仅用于列表展示
    QString name; //控件名，仅仅用于列表展示
    QString className; //类名，仅仅用于列表展示
    QString tip;     //控件提示
};


//插件信息
struct WidgetPluginMsg{
    QString filePath = ""; //插件文件名
    QLibrary* qLibPth = nullptr;
    uiWidgetMsg widgetMsg;
};



//属性接口
enum propertyType{
    Int,
    Rect,
    Double,
    Bool,
    Color,
    String,
    Size,
    Enum,
};

//属性子项信息
struct propertyMsg{
    QString name = "";                                  //属性名称
    propertyType type = propertyType::String;           //属性类型
    int value_Int = 0;                                  //属性值整数
    double value_Double = 0.0;                          //属性值浮点数
    bool value_Bool = false;                            //属性值布尔
    QColor value_Color = QColor(255,255,255,255);       //属性值颜色
    QString value_String = "";                          //属性值字符串
    QSize value_Size = QSize(0,0);                      //属性尺寸
    QRect value_Rect = QRect(0,0,0,0);                  //矩形信息
    QStringList value_Enum = {};                        //属性值的列表
    int value_EnumIndex = 0;                            //属性值的列表的索引
    bool enable = false;                                //属性是否启用
    QString sign = "基础属性";                           //标记，可以用于重名的属性区分

    bool operator<<(const propertyMsg& msg){  //只是判断类型与标记
        if(this->name != msg.name) return false;
        if(this->type != msg.type) return false;
        if(this->enable != msg.enable) return false;
        if(this->sign != msg.sign) return false;
        return true;
    }

    bool operator==(const propertyMsg& msg){  //只是判断值是否相同
        if(this->value_Int != msg.value_Int) return false;
        if(this->value_Double != msg.value_Double) return false;
        if(this->value_Bool != msg.value_Bool) return false;
        if(this->value_Color != msg.value_Color) return false;
        if(this->value_String != msg.value_String) return false;
        if(this->value_Enum != msg.value_Enum) return false;
        if(this->value_Size != msg.value_Size) return false;
        if(this->value_Rect != msg.value_Rect) return false;
        return true;
    }

    void operator=(QString str){                     //只是判断类型与标记
        value_Int = str.toInt();                     //属性值整数
        value_Double = str.toDouble();               //属性值浮点数
        value_Bool = false;                          //属性值布尔
        value_Color = QColor(255,255,255,255);       //属性值颜色
        value_String = str;                          //属性值字符串
        value_Enum = {};                             //属性值的列表
        value_Size = QSize(0,0);
        value_Rect = QRect(0,0,0,0);
        return;
    }
};


class Form_WidgetBox;
class Form_PropertyEditor;

extern WidgetPluginMsg* nowWidgetPluginMsg; //当前的控件插件信息
extern Form_WidgetBox* widgetBoxPtr; // 组件盒子指针
extern Form_PropertyEditor* PropertyEditorPtr;  //控件属性编辑器指针

#endif // GLOBALMSG_H
