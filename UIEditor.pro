QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++17

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    GlobalMsg.cpp \
    Widget/Widget_Base.cpp \
    Widget/Widget_Base_MainWindow.cpp \
    Widget/Widget_Button_WidgetItem.cpp \
    Widget/Widget_MdiArea.cpp \
    Widget/Widget_widgetList.cpp \
    Window/Form_EditorSpace.cpp \
    Window/Form_PropertyEditor.cpp \
    Window/Form_Roi.cpp \
    Window/Form_WidgetBox.cpp \
    main.cpp \
    qtpropertybrowser/qtbuttonpropertybrowser.cpp \
    qtpropertybrowser/qteditorfactory.cpp \
    qtpropertybrowser/qtgroupboxpropertybrowser.cpp \
    qtpropertybrowser/qtpropertybrowser.cpp \
    qtpropertybrowser/qtpropertybrowserutils.cpp \
    qtpropertybrowser/qtpropertymanager.cpp \
    qtpropertybrowser/qttreepropertybrowser.cpp \
    qtpropertybrowser/qtvariantproperty.cpp

HEADERS += \
    GlobalMsg.h \
    Widget/Widget_Base.h \
    Widget/Widget_Base_MainWindow.h \
    Widget/Widget_Button_WidgetItem.h \
    Widget/Widget_MdiArea.h \
    Widget/Widget_widgetList.h \
    Window/Form_EditorSpace.h \
    Window/Form_PropertyEditor.h \
    Window/Form_Roi.h \
    Window/Form_WidgetBox.h \
    qtpropertybrowser/qtbuttonpropertybrowser.h \
    qtpropertybrowser/qteditorfactory.h \
    qtpropertybrowser/qtgroupboxpropertybrowser.h \
    qtpropertybrowser/qtpropertybrowser.h \
    qtpropertybrowser/qtpropertybrowserutils_p.h \
    qtpropertybrowser/qtpropertymanager.h \
    qtpropertybrowser/qttreepropertybrowser.h \
    qtpropertybrowser/qtvariantproperty.h

FORMS += \
    Widget/Widget_Button_WidgetItem.ui \
    Widget/Widget_widgetList.ui \
    Window/Form_EditorSpace.ui \
    Window/Form_PropertyEditor.ui \
    Window/Form_Roi.ui \
    Window/Form_WidgetBox.ui

RESOURCES += \
    res.qrc

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
