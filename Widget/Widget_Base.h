
#ifndef WIDGET_BASE_H
#define WIDGET_BASE_H

#include "QWidget"
#include "QVector"
#include "../GlobalMsg.h"





class Widget_Base
{
public:



    //插件激发事件信息
    struct widgetEventMsg{
        QString eventName = ""; //事件名称
        QString eventReturn = "void"; //事件返回值
        QString eventArgs = ""; //事件参数表
    };


public:
    bool container = false; //是否为容器
    bool enable_geometry = true; //尺寸基本，设置尺寸是否可以被更改
    bool enable_minSize = true;  //尺寸最小
    bool enable_maxSize = true; //尺寸最大

    QVector<propertyMsg> propertyList;  //设置属性的列表
    QVector<widgetEventMsg> eventList; //事件列表

public:
    virtual QWidget* getWidgetInstance() = 0; //获取控件实例
    virtual QString getCodeStr() = 0; //获取代码字符串
    virtual void event_onWidgetSizeChange(QRect newSizeRect) = 0;//当前窗口尺寸被改变,newSizeRect为建议改变的值

    virtual void event_onSetChange(){return;}; //当属性被改变
    virtual void event_onChildJoin(QWidget* itemWidget){return;}; //当子项窗口被加入，此事件将在“container = true”后生效

};

#endif // WIDGET_BASE_H
