
#include "Widget_Base_MainWindow.h"
#include "QLayout"

#include "QPushButton"

Widget_Base_MainWindow::Widget_Base_MainWindow()
{
    container = true;  //容器，允许向内部添加控件
    enable_geometry = true; //尺寸基本，设置尺寸是否可以被更改
    enable_minSize = false;  //尺寸最小
    enable_maxSize = false; //尺寸最大

    propertyMsg t_strItem;
    t_strItem.name = "标题";
    t_strItem.type = propertyType::String;
    t_strItem.value_String = "主窗口";
    t_strItem.enable = true;
    t_strItem.sign = "窗体";
    propertyList.append(t_strItem);

    t_strItem.name = "边框";
    t_strItem.type = propertyType::Enum;
    t_strItem.value_Enum = {"默认","无边框"};
    t_strItem.enable = true;
    t_strItem.sign = "窗体";
    propertyList.append(t_strItem);
}


//获取控件实例
QWidget *Widget_Base_MainWindow::getWidgetInstance()
{
    if(widgetForm == nullptr){
        widgetForm = new QMainWindow;
        widgetForm->setObjectName("主窗口");
        widgetForm->move(10,10);
        widgetForm->setGeometry(0,0,440,300);
        widgetForm->layout()->setContentsMargins(0,0,0,0);
    }
    widgetForm->setAttribute(Qt::WA_StyledBackground); //脱离父窗口样式的覆盖
    widgetForm->setStyleSheet("background-color:rgb(240,240,240));");
    widgetForm->setWindowIcon(QIcon(":/Logo/icon/Logo/Logo_32.png"));
    widgetForm->setWindowTitle("主窗口");
    return widgetForm;
}


//获取代码字符串
QString Widget_Base_MainWindow::getCodeStr()
{
    return "";
}



//当前窗口尺寸被改变,newSizeRect为建议改变的值
void Widget_Base_MainWindow::event_onWidgetSizeChange(QRect newSizeRect)
{
    propertyList[0].value_Int = newSizeRect.top(); //刷新顶边属性
    propertyList[1].value_Int = newSizeRect.left(); //刷新左边属性
    propertyList[2].value_Size = QSize(newSizeRect.width(),newSizeRect.height()); //刷新默认宽高

    qDebug() << "主窗口建议改变的值:" <<newSizeRect;
}


//当属性被改变
void Widget_Base_MainWindow::event_onSetChange()
{
    return;
}


//当子项窗口被加入，此事件将在“container = true”后生效
void Widget_Base_MainWindow::event_onChildJoin(QWidget *itemWidget)
{
    return;
}

