
#ifndef WIDGET_BASE_MAINWINDOW_H
#define WIDGET_BASE_MAINWINDOW_H

#include "Widget_Base.h"
#include "QMainWindow"


class Widget_Base_MainWindow : public Widget_Base
{
public:
    Widget_Base_MainWindow();

private:
    QMainWindow* widgetForm = nullptr;

public:
    QWidget* getWidgetInstance() override; //获取控件实例
    virtual QString getCodeStr() override; //获取代码字符串


    void event_onWidgetSizeChange(QRect newSizeRect) override;//当前窗口尺寸被改变,newSizeRect为建议改变的值
    void event_onSetChange()  override; //当属性被改变
    void event_onChildJoin(QWidget* itemWidget)  override; //当子项窗口被加入，此事件将在“container = true”后生效

};

#endif // WIDGET_BASE_MAINWINDOW_H
