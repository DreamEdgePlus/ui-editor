#ifndef FORM_EDITORSPACE_H
#define FORM_EDITORSPACE_H

#include <QWidget>
#include "QMainWindow"
#include "ui_Form_EditorSpace.h"
#include "Form_Roi.h"
#include "Widget/Widget_Base.h"

namespace Ui {
class Form_EditorSpace;
}

class Form_EditorSpace : public QWidget
{
    Q_OBJECT

public:
    explicit Form_EditorSpace(QWidget *parent = nullptr);
    ~Form_EditorSpace();


    //类对象
    struct WidgetClassMsg{
        Widget_Base* classObj = nullptr;
        QWidget* formPtr = nullptr;
    };


public:
    QMdiSubWindow* BaseSubWindow;     //编辑器的主窗口
    Form_Roi* RoiWidget;                //选区绘画窗口指针
    QVector<WidgetClassMsg> widgetClassMsgLsit;   //子控件类指针


public:
    void event_onMouseDown(int x,int y,QWidget* formPtr); //当鼠标点击了某一个位置
    void event_onWidgetSizeChange(QWidget* widget,QRect changeRect); //当Widget尺寸选区大小被更改,注意newRect
    void event_onMdiAreaScrollChange(int dx, int dy); //当MdiArea滚动改变
    void event_onMdiAreaReSize(QResizeEvent *resizeEvent); //当MdiArea尺寸改变
    void event_onTreeItemClick(QWidget* widget); //当TreeItem被点击
    void event_onObjectRename(QWidget* widget,QString newName); //设置对象新名称
    void event_onPropertyChange(); //当属性改变

private slots:
    void on_splitter_splitterMoved(int pos, int index);//分割条比例调整

private:
    Ui::Form_EditorSpace *ui;
    bool ctrlDown = false;//Ctrl按下

    QRect getWidgetAbsolutePos(QWidget* form);//查询当前的窗体相对于基准窗口的位置信息
    QWidget* getWidgetFromPos(int x,int y); //根据坐标获取窗体指针
    QWidget* getContainerWidgetFromPos(int x,int y,WidgetClassMsg& wMsg); //根据坐标获取容器窗体指针
    WidgetClassMsg getWidgetClassMsg(QWidget* form); //根据窗口指针获取类信息
    QString getUniqueName(QString name); //根据当前的对象名获取唯一的对象名称


    void resizeEvent(QResizeEvent* event) override;//窗体尺寸被改变
    void keyPressEvent(QKeyEvent *event) override; //按钮被按下
    void keyReleaseEvent(QKeyEvent *event) override; //按钮被放开
};

#endif // FORM_EDITORSPACE_H
