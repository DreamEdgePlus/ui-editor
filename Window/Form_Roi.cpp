#include "Form_Roi.h"
#include "ui_Form_Roi.h"
#include "Form_EditorSpace.h"


#include "QMouseEvent"


Form_Roi::Form_Roi(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Form_Roi)
{
    ui->setupUi(this);
    this->setMouseTracking(true); //鼠标移动就会激发此事件


    editorSpace = (Form_EditorSpace*)parent;

    formPixmap = new QPixmap(this->geometry().width(),this->geometry().height()); //初始化画布
    formPixmap->fill(Qt::transparent); //画布透明化




}

Form_Roi::~Form_Roi()
{
    delete formPixmap;
    delete ui;
}


 //添加区域
void Form_Roi::roi_AddRec(QVector<widgetRecMsg> rois)
{
    this->startWidget = rois[rois.length() - 1].widget;
    this->startRect = rois[rois.length() - 1].rect;
    this->roiRecs.append(rois);
}


//清理区域
void Form_Roi::roi_ClearRec()
{
    this->roiRecs.clear();
}


//设置选取样式
void Form_Roi::roi_SetStyle(QColor rcolor, int wid)
{
    this->roiColor = rcolor;
    this->roiWidth = wid;
}


//微调改变选区的左边顶边的位置信息
void Form_Roi::roi_ChangeRecTopLeft(int dx, int dy)
{
    for(int a = 0;a<this->roiRecs.length();a++){
        QRect t_rec = this->roiRecs[a].rect;
        this->roiRecs[a].rect.setLeft(t_rec.left() + dx);
        this->roiRecs[a].rect.setTop(t_rec.top() + dy);
        this->roiRecs[a].rect.setWidth(t_rec.width());
        this->roiRecs[a].rect.setHeight(t_rec.height());
    }
}


//画选区
void Form_Roi::roi_DrawPix()
{
    QPainter t_painter(formPixmap);
    formPixmap->fill(Qt::transparent); //透明色填充

    QPen t_pen(Qt::PenStyle::NoPen);
    QBrush t_brush(this->roiColor); //刷子

    t_painter.setPen(t_pen);
    t_painter.setBrush(t_brush);

    for(auto value : this->roiRecs){
        QList<QRect> t_rosRecs;
        /*
         * 1 2 3
         * 4   5
         * 6 7 8
        */
        t_rosRecs.append({
            {value.rect.left() - this->roiWidth,value.rect.top() - this->roiWidth,this->roiWidth,this->roiWidth},  //1
            {value.rect.left() + (value.rect.width() - this->roiWidth) / 2,value.rect.top() - this->roiWidth,this->roiWidth,this->roiWidth},  //2
            {value.rect.left() + value.rect.width(),value.rect.top() - this->roiWidth,this->roiWidth,this->roiWidth},  //3
            {value.rect.left() - this->roiWidth,value.rect.top() + (value.rect.height() - this->roiWidth) / 2,this->roiWidth,this->roiWidth},  //4
            {value.rect.left() + value.rect.width(),value.rect.top() + (value.rect.height() - this->roiWidth) / 2,this->roiWidth,this->roiWidth},  //5
            {value.rect.left() - this->roiWidth,value.rect.top() + value.rect.height(),this->roiWidth,this->roiWidth},  //6
            {value.rect.left() + (value.rect.width() - this->roiWidth) / 2,value.rect.top() + value.rect.height(),this->roiWidth,this->roiWidth},  //7
            {value.rect.left() + value.rect.width(),value.rect.top() + value.rect.height(),this->roiWidth,this->roiWidth},  //8
        });

        t_painter.drawRects(t_rosRecs); //画出小的矩形
    }

    //绘制虚线
    if(virRect.width() != 0 && virRect.height() != 0){
        t_pen.setStyle(Qt::DashLine);
        t_pen.setColor(roiLineColor);
        t_pen.setWidth(1);
        t_brush.setStyle(Qt::BrushStyle::NoBrush);
        t_painter.setPen(t_pen);
        t_painter.setBrush(t_brush);
        t_painter.drawRect(virRect);
    }

    //this->update();


    //qDebug() << "111";
}



//获取矩形选区的数量
int Form_Roi::roi_GetRecLength()
{
    return roiRecs.length();
}




//获取选区窗口的所有指针信息
QVector<Form_Roi::widgetRecMsg> Form_Roi::roi_GetAllWidgetPtrs()
{
    return roiRecs;
}


//鼠标按下
void Form_Roi::mousePressEvent(QMouseEvent *event)
{
    startX = event->x();
    startY = event->y();

    this->mouseMoveEvent(event);
    mouseDown = true;
    editorSpace->event_onMouseDown(event->x(),event->y(),startWidget); //激发事件，并且传入点击的窗体，若不存在，则返回空
    this->update();
}


//鼠标抬起
void Form_Roi::mouseReleaseEvent(QMouseEvent *event)
{
    QWidget* t_widget = startWidget; //提前获取startWidget信息，后面再次执行mouseMoveEvent后将会被清除

    mouseDown = false;
    this->mouseMoveEvent(event);
    this->persionType = -1;//标记为未选中
    this->update(); //刷新页面

    if(virRect.width() != 0 && virRect.height() != 0){
        //通知尺寸改变
        editorSpace->event_onWidgetSizeChange(t_widget,QRect(virRect.left()-startRect.left(),virRect.top()-startRect.top(),virRect.width()-startRect.width(),virRect.height()-startRect.height())); //激活尺寸改变
    }
    virRect = QRect(0,0,0,0); //鼠标抬起，虚线框消失
    startWidget = nullptr;
}


//鼠标双击
void Form_Roi::mouseDoubleClickEvent(QMouseEvent *event)
{
    this->update();
    qDebug() << "mouseDoubleClickEvent";
}


//鼠标移动
void Form_Roi::mouseMoveEvent(QMouseEvent *event)
{
    int t_x = event->x();
    int t_y = event->y();

    //鼠标没有被按下
    if(mouseDown == false){
        this->setCursor(Qt::ArrowCursor); //默认鼠标指针

        struct t_rosMsg{
            QRect rect;
            int type = -1;
        };

        for(auto value : this->roiRecs){
            QList<t_rosMsg> t_rosRecs;
             /*
             * 1 2 3
             * 4   5
             * 6 7 8
             */
            if(t_x > value.rect.left() && t_x < value.rect.left() + value.rect.width() &&
                t_y > value.rect.top() && t_y < value.rect.top() + value.rect.height()){
                this->persionType = 0; //标记为矩形点击
                startRect = value.rect; //开始位置
                startWidget = value.widget; //开始的窗口指针
            }

            t_rosRecs.append({
                {{value.rect.left() - this->roiWidth,value.rect.top() - this->roiWidth,this->roiWidth,this->roiWidth},1},  //1
                {{value.rect.left() + (value.rect.width() - this->roiWidth) / 2,value.rect.top() - this->roiWidth,this->roiWidth,this->roiWidth},2},  //2
                {{value.rect.left() + value.rect.width(),value.rect.top() - this->roiWidth,this->roiWidth,this->roiWidth},3},  //3
                {{value.rect.left() - this->roiWidth,value.rect.top() + (value.rect.height() - this->roiWidth) / 2,this->roiWidth,this->roiWidth},4},  //4
                {{value.rect.left() + value.rect.width(),value.rect.top() + (value.rect.height() - this->roiWidth) / 2,this->roiWidth,this->roiWidth},5},  //5
                {{value.rect.left() - this->roiWidth,value.rect.top() + value.rect.height(),this->roiWidth,this->roiWidth},6},  //6
                {{value.rect.left() + (value.rect.width() - this->roiWidth) / 2,value.rect.top() + value.rect.height(),this->roiWidth,this->roiWidth},7},  //7
                {{value.rect.left() + value.rect.width(),value.rect.top() + value.rect.height(),this->roiWidth,this->roiWidth},8},  //8
            });

            for(int a=0;a<t_rosRecs.length();a++){
                if(t_x > t_rosRecs[a].rect.left() && t_x < t_rosRecs[a].rect.left() + t_rosRecs[a].rect.width() &&
                    t_y > t_rosRecs[a].rect.top() && t_y < t_rosRecs[a].rect.top() + t_rosRecs[a].rect.height()){

                    this->persionType = t_rosRecs[a].type; //矩形位置标记
                    startRect = value.rect; //开始位置
                    startWidget = value.widget; //开始的窗口指针

                    switch(t_rosRecs[a].type){
                    case 1:
                    case 8:{this->setCursor(Qt::SizeFDiagCursor); break;}
                    case 2:
                    case 7:{this->setCursor(Qt::SizeVerCursor); break;}
                    case 3:
                    case 6:{this->setCursor(Qt::SizeBDiagCursor); break;}
                    case 4:
                    case 5:{this->setCursor(Qt::SizeHorCursor); break;}
                    default:{this->setCursor(Qt::ArrowCursor); break;}
                    }
                    this->update();
                    return;
                }
            }
            this->setCursor(Qt::ArrowCursor);
        }
    }
    else{ //鼠标是按下的状态
        virRect = startRect;
        switch(this->persionType){
        case 0:{
            virRect.setLeft(startRect.left() + (t_x - startX));
            virRect.setTop(startRect.top() + (t_y - startY));
            virRect.setWidth(startRect.width());
            virRect.setHeight(startRect.height());
            break;
        }
        case 1:{
            virRect.setLeft(startRect.left() + (t_x - startX));
            virRect.setTop(startRect.top() + (t_y - startY));
            break;
        }
        case 2:{
            virRect.setTop(startRect.top() + (t_y - startY));
            break;
        }
        case 3:{
            virRect.setTop(startRect.top() + (t_y - startY));
            virRect.setWidth(startRect.width() + (t_x - startX));
            break;
        }
        case 4:{
            virRect.setLeft(startRect.left() + (t_x - startX));
            break;
        }
        case 5:{
            virRect.setWidth(startRect.width() + (t_x - startX));
            break;
        }
        case 6:{
            virRect.setLeft(startRect.left() + (t_x - startX));
            virRect.setHeight(startRect.height() + (t_y - startY));
            break;
        }
        case 7:{
            virRect.setHeight(startRect.height() + (t_y - startY));
            break;
        }
        case 8:{
            virRect.setWidth(startRect.width() + (t_x - startX));
            virRect.setHeight(startRect.height() + (t_y - startY));
            break;
        }
        default:{break;}
        }

        //将高度与宽度调整，防止宽高为负数
        if(virRect.width() < 0){virRect.setWidth(0);}
        if(virRect.height() < 0){virRect.setHeight(0);}

        //刷新roi
        this->update();
    }
}


//重写绘制方法
void Form_Roi::paintEvent(QPaintEvent *event)
{
    roi_DrawPix();
    QPainter t_painter(this);
    t_painter.drawPixmap(0,0,*formPixmap);
}


//尺寸改变
void Form_Roi::resizeEvent(QResizeEvent *event)
{
    QPixmap* t_pix = new QPixmap(this->width(),this->height());  //将原来的Pix信息复制到新的pix
    t_pix->fill(Qt::transparent);
    QPainter t_painter(t_pix);
    t_painter.drawPixmap(0,0,*formPixmap);
    delete formPixmap;
    formPixmap = t_pix;
}


