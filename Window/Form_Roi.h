#ifndef FORM_ROI_H
#define FORM_ROI_H

#include <QWidget>
#include "QPainter"


class Form_EditorSpace;


namespace Ui {
class Form_Roi;
}

class Form_Roi : public QWidget
{
    Q_OBJECT

public:
    explicit Form_Roi(QWidget *parent = nullptr);
    ~Form_Roi();


public:
    struct widgetRecMsg{
        QWidget* widget; //窗口指针
        QRect rect; //窗口矩形
    };



private:
    Ui::Form_Roi *ui;
    QVector<widgetRecMsg> roiRecs; //roi矩形组
    QColor roiColor = QColor(0,0,128); //选区颜色
    QColor roiLineColor = QColor(100,100,100); //选区颜色
    int roiWidth = 6; //边框点的尺寸
    QRect virRect = QRect(0,0,0,0); //虚线框选，用于拖拽，或者缩放
    QPixmap* formPixmap = nullptr; //窗口绘画内容

    Form_EditorSpace* editorSpace; //父组件，编辑空间

    int persionType = -1; //1-8为矩形区域，0为选中，-1为未选中
    QRect startRect; //矩形开始的位置
    int startX;
    int startY;
    QWidget* startWidget = nullptr; //一定要赋予初值，否则随机的数值会影响事件的判断

    bool mouseDown = false; //鼠标按下

public:
    void roi_AddRec(QVector<widgetRecMsg> rois); //添加区域
    void roi_ClearRec(); //清理区域
    void roi_SetStyle(QColor roiColor,int wid = 6); //设置选取样式
    void roi_ChangeRecTopLeft(int dx,int dy); //微调改变选区的左边顶边的位置信息
    void roi_DrawPix(); //画选区
    int roi_GetRecLength(); //获取矩形选区的数量
    QVector<widgetRecMsg> roi_GetAllWidgetPtrs(); //获取选区窗口的所有指针信息


protected:
    void mousePressEvent(QMouseEvent *event); //鼠标按下
    void mouseReleaseEvent(QMouseEvent *event); //鼠标放开
    void mouseDoubleClickEvent(QMouseEvent *event); //鼠标双击
    void mouseMoveEvent(QMouseEvent *event); //鼠标移动


    void paintEvent(QPaintEvent *event) override; //重写绘制方法
    void resizeEvent(QResizeEvent *event) override; //尺寸改变

};

#endif // FORM_ROI_H
