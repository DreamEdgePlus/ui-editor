#include "QDir"
#include "QFileInfo"

#include "GlobalMsg.h"
#include "Form_WidgetBox.h"
#include "ui_Form_WidgetBox.h"
#include "Widget/Widget_Button_WidgetItem.h"


Form_WidgetBox::Form_WidgetBox(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Form_WidgetBox)
{
    ui->setupUi(this);
    //ui->listWidget->setViewMode(QListWidget::ViewMode::IconMode); //图像模式
    //ui->listWidget->setResizeMode(QListView::Adjust); //自动对其

    //连接按钮被按下
    connect(ui->listWidget,&QListWidget::itemClicked,[this](QListWidgetItem * item){
        Widget_Button_WidgetItem* t_widget = (Widget_Button_WidgetItem*)ui->listWidget->itemWidget(item);
        QString t_title = t_widget->GetTitle();
        QString t_sign = t_widget->GetSign();
        onItemDown(t_title,t_sign);
    });

}

Form_WidgetBox::~Form_WidgetBox()
{
    delete ui;
}


QSize Form_WidgetBox::sizeHint() const
{
    return QSize(180, 900); /* 在这里定义dock的初始大小 */
}



//初始化Widget
void Form_WidgetBox::Init(QString dirPath, QString plgSuffix)
{
    this->LoadWidget(dirPath,plgSuffix); //加载插件信息
    this->addItem(QPixmap(":/WidgetBox/icon/WidgetBox/BlueIcons/Cursor_16x.png"),"指针","控件选择指针，此项目不是有效控件");

    for(auto value : List_widgetPlg){
        this->addItem(value.widgetMsg.pix,value.widgetMsg.name,value.widgetMsg.tip);
    }
}





//加载控件
void Form_WidgetBox::LoadWidget(QString dirPath, QString plgSuffix)
{
    QVector<QString> t_pathList;
    this->findPlugin(dirPath,t_pathList,plgSuffix); //扫描plg插件
    for(int a=0;a<t_pathList.length();a++){
        WidgetPluginMsg t_plgMsg; //插件信息
        t_plgMsg.filePath = t_pathList[a];

        QLibrary* t_lib = new QLibrary(t_plgMsg.filePath); //加载插件（以动态库的形式加载插件，插件本身就是一个动态库）
        if(t_lib->load()){
            typedef uiWidgetMsg (*fun)();
            fun t_getIns = (fun)t_lib->resolve("getWidgetMsg"); //加载实例对象
            if(t_getIns != nullptr){
                //已经获取了实例
                t_plgMsg.qLibPth = t_lib; //保存当前的QLibrary对象
                t_plgMsg.widgetMsg = t_getIns(); //获取instance实例
                this->List_widgetPlg.append(t_plgMsg); //添加实例插件
                continue;
            }
        }
        delete t_lib;
    }
}



//选择项目
void Form_WidgetBox::selectItem(int index)
{
    ui->listWidget->setCurrentRow(index);
}



//查找寻找插件文件
void Form_WidgetBox::findPlugin(QString path, QVector<QString> &retFiles, QString plgSuffix)
{
    QDir t_dir(path);
    if(!t_dir.exists()) return;
    QFileInfoList t_infoListDir = t_dir.entryInfoList(QDir::Dirs|QDir::NoDotAndDotDot); //只检查有多少目录
    for(int a = 0;a<t_infoListDir.length();a++){
        if(t_infoListDir[a].exists()){
            QDir t_dirFiles(t_infoListDir[a].filePath());
            QFileInfoList t_infoListFile = t_dirFiles.entryInfoList(QDir::Files|QDir::NoDotAndDotDot); //只检查有多少文件
            for(int b = 0;b<t_infoListFile.length();b++){
                if(t_infoListFile[b].isFile() && t_infoListFile[b].suffix() == plgSuffix){ //判断是否为工程模板文件
                    retFiles.append(t_infoListFile[b].absoluteFilePath());
                }
            }
        }
    }
}





//添加项目
void Form_WidgetBox::addItem(QPixmap pixmap, QString title, QString sign)
{
    QListWidgetItem* t_item = new QListWidgetItem(ui->listWidget);
    Widget_Button_WidgetItem* t_widget = new Widget_Button_WidgetItem(pixmap,title,sign,ui->listWidget);
    t_item->setSizeHint(QSize(150,32));
    t_item->setToolTip(sign);
    ui->listWidget->addItem(t_item);
    ui->listWidget->setItemWidget(t_item,t_widget);
    //ui->listWidget->setMovement(QListWidget::Static); //防止手动移动改变了位置，已经在UI编辑器里面设置
}


//Item被点击
void Form_WidgetBox::on_listWidget_itemClicked(QListWidgetItem *item)
{
    int t_index = ui->listWidget->indexFromItem(item).row();

    if(t_index < 1 || t_index > this->List_widgetPlg.length()){ //检查是否超出容器能力范围
        nowWidgetPluginMsg = nullptr;
    }
    else{
        nowWidgetPluginMsg = &this->List_widgetPlg[t_index - 1]; //赋予控件的插件信息
    }



}

