/********************************************************************************
** Form generated from reading UI file 'Form_MainWindow.ui'
**
** Created by: Qt User Interface Compiler version 6.5.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FORM_MAINWINDOW_H
#define UI_FORM_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Form_MainWindow
{
public:
    QWidget *centralwidget;
    QMenuBar *menubar;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *Form_MainWindow)
    {
        if (Form_MainWindow->objectName().isEmpty())
            Form_MainWindow->setObjectName("Form_MainWindow");
        Form_MainWindow->resize(800, 600);
        centralwidget = new QWidget(Form_MainWindow);
        centralwidget->setObjectName("centralwidget");
        Form_MainWindow->setCentralWidget(centralwidget);
        menubar = new QMenuBar(Form_MainWindow);
        menubar->setObjectName("menubar");
        menubar->setGeometry(QRect(0, 0, 800, 26));
        Form_MainWindow->setMenuBar(menubar);
        statusbar = new QStatusBar(Form_MainWindow);
        statusbar->setObjectName("statusbar");
        Form_MainWindow->setStatusBar(statusbar);

        retranslateUi(Form_MainWindow);

        QMetaObject::connectSlotsByName(Form_MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *Form_MainWindow)
    {
        Form_MainWindow->setWindowTitle(QCoreApplication::translate("Form_MainWindow", "MainWindow", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Form_MainWindow: public Ui_Form_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FORM_MAINWINDOW_H
